this week is using makefile.
only including new things and not git commands.

##bash commands
vim Makefile    // create Makefile
cat -vTE Makefile   // check if contents of file are formatted properly

// follow instructions in makefile(labelled as feature) to create all directories and files
// main directory is called "test_output"
make feature NAME=test_output   

/created test_outputs.c

gcc -Wall -pedantic test_outputs.c -o test_outputs  //compile

/created op_test file

// test_outputs.c contains code which reads files so need input file
// run "test_outputs" with "op_test" as input file
 ./test_outputs op_test

##c program
reads command and argument from op_test
executes command
read expected output and compare with actual output
if different print error message

#op_test
wc -l	//count the number of lines
test_outputs.c	//in this file
108 test_outputs.c	//expect 108 lines
