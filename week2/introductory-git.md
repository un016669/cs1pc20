/ git init
creates directory with git project

/git add
stage files with uncommitted changes

/git commit
commit staged files

/git status
show staged files and files with uncommitted changes

/git push
push local project to remote 

/git branch 
create new branch

/git checkout
change current branch

/git merge <branch name>
merge contents of current branch and selected branch

/git clone 
clone from project from remote or local directory

/git remote
many functions relating to remote repository
set-url
add

have to add origin for some reason

/git stash
store current state of working directory and revert to HEAD

/git config
set username and password ++ other things
