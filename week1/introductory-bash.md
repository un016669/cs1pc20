1/ mkdir
creates folder inside current directory
to make multiple nested folders use $HOME/folder1/folder2

2/ cd 
change current directory
cd ~ to go to home directory

3/ rm
remove file
rm -r to remove directory ; recursive remove

4/ echo
prints input
can print string or variables

6/ cat
read file and output

7/ which 
returns path name of commands/links

8/ man
manual for any command

9/ ./
run code output

bc 
calculate program

cal
calender program

gcc
gnu compiler 

git
source control program

$
reference variable

> 
write into file
ex : "HelloWorld" > readme

<
output from file

;
run multiple commands

| 
run with program
1+1+1 | bc

\n
new line
