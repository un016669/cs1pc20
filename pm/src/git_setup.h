#ifndef GIT_SETUP_H
#define GIT_SETUP_H

void InitializeGit(char *name);
void CreateRemote(char *name, char *url);
void CreateBranch(char *name);

#endif