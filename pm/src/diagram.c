#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include "time_tracking.h"

#define MAX_PATH_LENGTH 256
#define COMMAND_LENGTH 512
#define MAX_FILE_LENGTH 64
#define MAX_DIR_LENGTH 1024

//exclude any folders that are default 
//not using tagging to identify folders because features can exist without tag
int ValidFolder(char *name)
{
    if (name[0] == '.')
    {
        return 1;
    }
    if (strcmp(name, "src") == 0)
    {
        return 1;
    }
    if (strcmp(name, "bin") == 0)
    {
        return 1;
    }
    if (strcmp(name, "docs") == 0)
    {
        return 1;
    }
    if (strcmp(name, "lib") == 0)
    {
        return 1;
    }
    if (strcmp(name, "tests") == 0)
    {
        return 1;
    }

    return 0;
}

//given path, outputs name of deepest directory
void DirectoryName(char *outName, char *path)
{
    for (int i = strlen(path); i >= 0; i--)
    {
        if(path[i] == '/')   //find index where file name terminates
        {
            memcpy(outName, &path[i+1], strlen(path) - i);
            return;
        }
    }
}

//recursive search through folders
//write every folder name to .puml file
int WriteTree(char *filePath, char *path, int depth)
{
    FILE *fp;
    fp = fopen(filePath, "a");
    
    char line[128];
    char name[64];
    char stars[32];
    stars[0] = '\0';
    DirectoryName(name, path);
    
    for (int i = 0; i < depth; i++)
    {
        strcat(stars, "*");
    }
    

    FILE *timeFile;
    char timeFilePath[128];
    snprintf(timeFilePath, sizeof timeFilePath, "%s/.pm_time", path);
    timeFile = fopen(timeFilePath, "r");
    if (timeFile != NULL)   //if folder is tracked also display estimated workloads
    {
        snprintf(line , sizeof line, "%s:%s\n", stars, name);
        fputs(line, fp);
        fgets(line, sizeof line, timeFile);
        fputs(line, fp);
        fgets(line, sizeof line, timeFile);
        int endChar = strlen(line) - 1;
        line[endChar] = ';';
        line[endChar + 1] = '\n';
        line[endChar + 2] = '\0';
        fputs(line, fp);
        fclose(timeFile);
    }
    else
    {
        snprintf(line , sizeof line, "%s %s\n", stars, name);
        fputs(line, fp);
    }
    
    fclose(fp);

    DIR *dir;
    struct dirent *dp;

    dir = opendir(path);
    if (dir == NULL)
    {
        printf("Could not open directory.\n");
        return 1;
    }
    while ((dp = readdir(dir)) != NULL)
    {
        char subPath[MAX_PATH_LENGTH + 100];
        snprintf(subPath, sizeof subPath, "%s/%s", path, dp->d_name);

        //search subdirectory
        //ignore hidden directories
        if (dp->d_type == DT_DIR && ValidFolder(dp->d_name) == 0)
        {
            WriteTree(filePath, subPath, depth + 1);   
        }    
    }
    closedir(dir);
    return 0;
}

// another recursive function for writing gantt file ;-;
// parent is a folder name of bar to add to end of
int WriteGantt(char *filePath, char *path, char *parent)
{
    //writing to file
    FILE *fp;
    fp = fopen(filePath, "a");  //append to .puml file
    
    char line[128];
    char name[64];
    DirectoryName(name, path);

    FILE *timeFile;
    char timeFilePath[128];
    snprintf(timeFilePath, sizeof timeFilePath, "%s/.pm_time", path);
    timeFile = fopen(timeFilePath, "r");
    if (timeFile != NULL)   //if folder is tracked add bars to file
    {   
        //get time values from file
        fgets(line, sizeof line, timeFile);
        int selfTime = ValueFromLine(line);
        fgets(line, sizeof line, timeFile);
        int totalTime = ValueFromLine(line);

        //create bar entities
        if (totalTime != selfTime)  //if contains subworkloads create overhanging bar
        {
            snprintf(line, sizeof line, "[%s total] lasts %d days\n", name, totalTime);
            fputs(line, fp);
        }
        snprintf(line, sizeof line, "[%s] lasts %d days\n", name, selfTime);
        fputs(line, fp);

        if(parent[0] != '\0')  // position bars at end of previous
        {
            if (totalTime != selfTime)
            {
                snprintf(line, sizeof line, "[%s total] starts at [%s]'s end\n", name, parent);
                fputs(line, fp);
            }
            snprintf(line, sizeof line, "[%s] starts at [%s]'s end\n", name, parent);
            fputs(line, fp);        
        }
        fclose(timeFile);

        //shifting pointer to current's end
        if (totalTime != selfTime)  // if cumulative at to total end
        {
            char totalBar[128];
            snprintf(totalBar, sizeof totalBar, "%s total", name);
            strcpy(parent, totalBar);  
        }
        else
        {
            strcpy(parent, name);
        }  
    }
    fclose(fp);

    //recurse into subdirs
    DIR *dir;
    struct dirent *dp;

    char nextBar[128];  //stores latest added bar for positioning
    nextBar[0] = '\0';  //initialize as empty/"null" string
    char *p_nextBar = &nextBar[0]; // use char* to allow for null values 
    if(IsTracked(path) == 0)
    {
        strcpy(p_nextBar, name);      //pointer starts at current dir
    }
    else
    {
        strcpy(p_nextBar, parent);    // if theres gap in tracked folders chain, bridge between parent and subdir
    }

    dir = opendir(path);
    if (dir == NULL)
    {
        printf("Could not open directory.\n");
        return 1;
    }
    while ((dp = readdir(dir)) != NULL)
    {
        char subPath[MAX_PATH_LENGTH + 100];
        snprintf(subPath, sizeof subPath, "%s/%s", path, dp->d_name);

        //search subdirectory
        //ignore hidden directories
        if (dp->d_type == DT_DIR && ValidFolder(dp->d_name) == 0)
        {
            WriteGantt(filePath, subPath, p_nextBar);   // nextBar is contnually updated
        }    
    }
    closedir(dir);

    return 0;
}


void CreateTreeSvg(char *projectName)
{
    //create .puml file
    FILE *fp;
    char filePath[150];
    char path[128];
    getcwd(path, sizeof path);
    snprintf(filePath, sizeof filePath, "%s/%s_tree.puml", path, projectName);

    fp = fopen(filePath, "w");
    fputs("@startwbs\n", fp);
    fclose(fp);

    char startPath[256];
    snprintf(startPath, sizeof startPath, "./%s", projectName);
    WriteTree(filePath, startPath, 1);

    fp = fopen(filePath, "a");
    fputs("@endwbs\n", fp);
    fclose(fp);

    //export .puml to .svg
    FILE *pCommand;
    char command[256];

    //run plantuml.jar located in home directory with .puml file as input
    snprintf(command, sizeof command, "java -jar %s/plantuml.jar -tsvg %s",getenv("HOME"), filePath);
    printf("%s\n", command);
    pCommand = popen(command, "r");
    pclose(pCommand);
}

void CreateGanttSvg(char *projectName)
{   
    //create .puml file
    FILE *fp;
    char filePath[150];
    char path[128];
    getcwd(path, sizeof path);
    snprintf(filePath, sizeof filePath, "%s/%s_gantt.puml", path, projectName);

    fp = fopen(filePath, "w");
    fputs("@startgantt\n", fp);
    fclose(fp);

    char startPath[256];
    char nextBar[128];  //stores latest added bar for positioning
    nextBar[0] = '\0';  //initialize as empty/"null" string
    char *p_nextBar = &nextBar[0]; // use char* to allow for null values 
    snprintf(startPath, sizeof startPath, "./%s", projectName);
    WriteGantt(filePath, startPath, p_nextBar);

    fp = fopen(filePath, "a");
    fputs("@endwbs\n", fp);
    fclose(fp);

    //export .puml to .svg
    FILE *pCommand;
    char command[256];

    //run plantuml.jar located in home directory with .puml file as input
    snprintf(command, sizeof command, "java -jar %s/plantuml.jar -tsvg %s",getenv("HOME"), filePath);
    printf("%s\n", command);
    pCommand = popen(command, "r");
    pclose(pCommand);

}