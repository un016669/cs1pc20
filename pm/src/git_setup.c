#include <stdio.h>
#include <unistd.h>

#define COMMAND_LENGTH 512
#define PATH_LENGTH 256

void InitializeGit(char *name, char *url)
{
    FILE *p_command; //temp file to hold pointer
    char path[PATH_LENGTH];
    snprintf(path, sizeof path, "./%s", name);
    char command[COMMAND_LENGTH]; //temp string to hold commands

    snprintf(command, sizeof command, "%s/.gitignore", path); //file path
    p_command = fopen(command, "w");
    fclose(p_command);
    printf("%s\n", command);

    //git init
    snprintf(command, sizeof command, "git init %s", name); //format command
    p_command = popen(command, "r"); //run command
    pclose(p_command);
    printf("%s\n", command);

    //git add files
    snprintf(command, sizeof command, "git -C %s add -A", path); //format command
    p_command = popen(command, "r"); //run command
    pclose(p_command);
    printf("%s\n", command);

    //git initial commit
    snprintf(command, sizeof command, "git -C %s commit -m \"initial commit\"", path); //format command
    p_command = popen(command, "r"); //run command
    pclose(p_command);
    printf("%s\n", command);

    

    //git push --set-upstream https://csgitlab.reading.ac.uk/un016669/test5 master

    // p_command = popen(command, "r"); //run command
    // pclose(p_command);
}

void CreateRemote(char *name, char *url)
{
    FILE *p_command; //temp file to hold pointer
    char path[PATH_LENGTH];
    snprintf(path, sizeof path, "./%s", name);
    char command[COMMAND_LENGTH]; //temp string to hold commands

    //create remote repo
    snprintf(command, sizeof command, "git -C %s push --set-upstream %s/%s master", path, url, name); //format command
    p_command = popen(command, "r"); //run command
    pclose(p_command);
    printf("%s\n", command);
}

void CreateBranch(char *name)
{
    FILE *p_command;
    char command[COMMAND_LENGTH];
    snprintf(command, sizeof command, "git branch %s", name);
    p_command = popen(command, "r");
    pclose(p_command);
    printf("%s\n", command);
}