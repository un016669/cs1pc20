#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>

#define PATH_LENGTH 64

void CreateDirectories(char *name)
{
    char path[PATH_LENGTH];
    
    //make main dir
    mkdir(name,0777);

    //make sub dirs
    snprintf(path, sizeof path, "%s/%s", name, "bin");
    mkdir(path,0777);
    snprintf(path, sizeof path, "%s/%s", name, "docs");
    mkdir(path,0777);
    snprintf(path, sizeof path, "%s/%s", name, "lib");
    mkdir(path,0777);
    snprintf(path, sizeof path, "%s/%s", name, "src");
    mkdir(path,0777);
    snprintf(path, sizeof path, "%s/%s", name, "tests");
    mkdir(path,0777);
}