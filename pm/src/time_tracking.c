#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include "tags.h"
#include "name_checking.h"
#include "time_tracking.h"
#include "diagram.h"

//take dir and return parent dir
void ParentDir(char *outDir)
{
    for(int i = strlen(outDir); i >= 0; i--)
    {
        if (outDir[i] == '/')
        {
            outDir[i] = '\0';
            return;
        }
    }
}

//find root timefile dir relative to cd
//so that can do flood search from root to update time dependencies
void GetRoot(char *outDir)
{
    char dir[256];
    getcwd(dir, sizeof dir);

    char parentDir[256];
    
    while (1)   //keep searching out until finding a folder without .pm_time
    {
        strcpy(parentDir, dir);
        ParentDir(parentDir);
        strcat(parentDir, "/.pm_time");
        if(IsUnique(parentDir) == true)
        {
            strcpy(outDir, dir);
            return;
        }
        ParentDir(dir);
    }
}

int StrToInt(char *string)
{
    return (int) strtol(string, (char **)NULL, 10);
}

//gets int value from formatted string
//"selftime:6" returns 6
int ValueFromLine(char *line)
{
    char temp[128];
    char numStr[128];
    strcpy(temp, line);
    for (int i = 0; i < strlen(temp); i++)
    {
        if (temp[i] == ':')
        {
            memcpy(numStr, &temp[i+1], strlen(temp) - i);
            return StrToInt(numStr);
        }
    }
    return -1;
}

//check if dir has time file
int IsTracked(char *path)
{
    char filePath[128];
    strcpy(filePath, path);
    strcat(filePath, "/.pm_time");

    FILE *fp;
    fp = fopen(filePath, "r");
    if (fp == NULL)
    {
        
        return 1;
    }
    fclose(fp);
    return 0;
}

//adds the times of subfeatures to parent feature recursively
int UpdateTotalTime(int *srcTime, char *path)
{
    int selfTime;
    int totalTime;

    //read time file and keep copy of contents to rewrite it 
    FILE *fp;
    char fileName[128];
    char selfTimeLine[128];
    snprintf(fileName, sizeof fileName, "%s/.pm_time", path);
    fp = fopen(fileName, "r");
    fgets(selfTimeLine, sizeof selfTimeLine, fp);
    fclose(fp);

    selfTime = ValueFromLine(selfTimeLine);
    totalTime = selfTime;

    DIR *dir;
    struct dirent *dp;

    dir = opendir(path);
    if (dir == NULL)
    {
        printf("Could not open directory.\n");
        return 1;
    }
    while ((dp = readdir(dir)) != NULL)
    {
        char subPath[260];
        snprintf(subPath, sizeof subPath, "%s/%s", path, dp->d_name);
        if (dp->d_type == DT_DIR && ValidFolder(dp->d_name) == 0 && IsTracked(subPath) == 0)
        {
            UpdateTotalTime(&totalTime, subPath);   //add subdirectory times to total
        }    
    }
    closedir(dir);

    //update time file
    char line[64];
    snprintf(line, sizeof line, "self_time:%d\n", selfTime);
    fp = fopen(fileName, "w");
    fputs(line, fp);
    snprintf(line, sizeof line, "total_time:%d\n", totalTime);
    fputs(line, fp);
    fclose(fp);

    if (srcTime != NULL)    //no parent to add to if at root call
    {
        *srcTime += totalTime;   //add self to parent
    }
    return 0;
}


//create a file that stores estimated time to completion
// if file already exists then overrite its contents
//time measured in days

/*example .pm_time file format
selftime:5\n
totaltime:17\n    //time including subfeatures
*/

void TrackFolder(int completionTime)
{
    char filePath[256];
    getcwd(filePath, sizeof filePath);
    strcat(filePath, "/.pm_time");
    char line[64];
    snprintf(line, sizeof line, "self_time:%d\n", completionTime);
    FILE *fp;
    fp = fopen(filePath, "w");
    fputs(line, fp);
    snprintf(line, sizeof line, "total_time:%d\n", completionTime);
    fputs(line, fp);
    fclose(fp);

    char root[128];
    GetRoot(root);
    UpdateTotalTime(NULL, root);  //update total times of parent dirs
}

