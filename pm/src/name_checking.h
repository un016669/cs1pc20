#include <stdbool.h>
#ifndef NAME_CHECKING_H
#define NAME_CHECKING_H

bool IsUnique(char *name);
bool IsValidDirectoryName(char *name);

#endif