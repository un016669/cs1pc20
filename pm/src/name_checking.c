#include <dirent.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#define COM_CREATE_PROJECT "create_project"
#define COM_ADD_FEATURE "add_feature"

bool IsUnique(char *name)
{
    FILE *file;
    file = fopen(name, "r");

    if (file != NULL)   //check if file/folder with this name exists.
    {
        fclose(file);
        return false;
    }
    
    return true;
}

bool IsValidDirectoryName(char *name)
{
    for (int i = 0; i < strlen(name); i++)
    {
        if (name[i] == ' ')
        {
            return false;
        } 
    }
    return true;
}