#ifndef TIME_TRACKING_H
#define TIME_TRACKING_H

#define TIME_FILE_NAME ".pm_time"
void TrackFolder(int completionTime);
int ValueFromLine(char *line);
int IsTracked(char *path);
#endif