#ifndef TAG_H
#define TAG_H

#define TAG_FILE_NAME ".pm_tag"

void AddTag(char *tagName);
int FindTag(char *path, char *tagName);
int RenameByTag(char *tagName, char *newName);
int MoveByTags(char *startTag, char *endTag);

#endif