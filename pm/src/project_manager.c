#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "create_directories.h"
#include "git_setup.h"
#include "name_checking.h"
#include "tags.h"
#include "diagram.h"
#include "time_tracking.h"

int main(int argc, char **argv)
{

    if (argc >= 2)
    {
        char *command = argv[1];

        //pm create_project <name>      to just create local repo
        //pm create_project <name> <url>    to create remote repo   
        //url example = https://csgitlab.reading.ac.uk/<username>
        if(strcmp(command, "create_project") == 0)
        {
            if (argc >= 3 && argc <= 4)
            {
                char *projectName = argv[2];         

                if(IsUnique(projectName))
                {
                    if(IsValidDirectoryName(projectName))
                    {
                        CreateDirectories(projectName);
                        InitializeGit(projectName);
                        
                        if (argc == 4)
                        {
                            char *url = argv[3];
                            CreateRemote(projectName, url);
                        }
                    }
                    else
                    {
                        printf("Invalid directory name.\n");
                    }
                    
                }
                else
                {
                    printf("A folder/file of that name already exists.\n");
                }
            }
            else
            {
                printf("wrong number of args\n");
            }     
        }

        //pm add_feature <feature name>
        if(strcmp(command, "add_feature") == 0)
        {
            if (argc == 3)
            {
                char *featureName = argv[2];
                if(IsUnique(featureName))
                {
                    if(IsValidDirectoryName(featureName))
                    {
                        CreateBranch(featureName);
                        CreateDirectories(featureName);
                    }
                    else
                    {
                        printf("Invalid directory name.\n");
                    }
                } 
                else
                {
                    printf("A folder/file of that name already exists.\n");
                }
            }
            else
            {
                printf("wrong number of args\n");
            }
              
        }

        //pm add_tag <tag name>
        if(strcmp(command, "add_tag") == 0)
        {
            if(IsUnique(TAG_FILE_NAME))
            {
                AddTag(argv[2]);
            }
            else
            {
                printf("Already a tag for this folder.\n");
            }
        }

        //pm find_tag <tag name>
        if(strcmp(command, "find_tag") == 0)
        {   
            char buffPath[256];
            stpcpy(buffPath, ".");
            if (FindTag(buffPath, argv[2]) == 0)
            {
                printf("found tag in path : \"%s\"\n", buffPath);
            }
            else
            {
                printf("tag not found.\n");
            }
        }

        //pm rename_by_tag <tag name> <new folder name>
        if (strcmp(command, "rename_by_tag") == 0)
        {
            RenameByTag(argv[2], argv[3]);
        }

        //pm move_by_tag <source tag> <destination tag>
        if (strcmp(command, "move_by_tag") == 0)
        {
            MoveByTags(argv[2], argv[3]);
        }

        //pm output_tree <project name>
        if (strcmp(command, "output_tree") == 0)
        {
            CreateTreeSvg(argv[2]);
        }

        //pm output_gantt <project name>
        if (strcmp(command, "output_gantt") == 0)
        {
            CreateGanttSvg(argv[2]);
        }

        //pm track_time <days>
        //set estimate for number of days to complete feature in .pm_time file
        //total time in .pm_time will stay updated with added files
        if (strcmp(command, "track_time") == 0)
        {
            int days = (int) strtol((argv[2]), (char **)NULL, 10);
            TrackFolder(days);
        }
    }

    return 0;
}