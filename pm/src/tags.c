//for functions relating to tags
//adding tags and finding path of tag
#include "tags.h"
#include <stdio.h>
#include <dirent.h>
#include <string.h>

#define MAX_PATH_LENGTH 256
#define COMMAND_LENGTH 512
#define MAX_FILE_LENGTH 64

void AddTag(char *tagName)
{
    FILE *fp;
    fp = fopen(TAG_FILE_NAME, "w");
    fputs(tagName, fp);
    fclose(fp);
}

//recursive function to search directory and recursive subdirectories
//call with "." as starting path
//output in path
//0 if found ; 1 if not
int FindTag(char *path, char *tagName)
{
    DIR *dir;
    struct dirent *dp;

    dir = opendir(path);
    if (dir == NULL)
    {
        printf("Could not open directory.\n");
        return 1;
    }
    while ((dp = readdir(dir)) != NULL)
    {
        char subPath[MAX_PATH_LENGTH + 100];
        snprintf(subPath, sizeof subPath, "%s/%s", path, dp->d_name);

        if (dp->d_type == DT_REG && strcmp(dp->d_name, TAG_FILE_NAME) == 0)
        {
            FILE *tagFile;
            char fileContents[MAX_FILE_LENGTH];
            tagFile = fopen(subPath, "r");
            fgets(fileContents, sizeof fileContents, tagFile);
            if (strcmp(fileContents, tagName) == 0)     //base case to unwind if found
            {
                closedir(dir);
                return 0;
            }
        }

        //search subdirectory
        //ignore hidden directories
        if (dp->d_type == DT_DIR && dp->d_name[0] != '.')
        {
            
            if (FindTag(subPath, tagName) == 0)      //recursive call
            {
                strcpy(path, subPath);      //pass found path up the chain
                closedir(dir);
                return 0;     //return found value
            }
        }    
    }
    closedir(dir);
    return 1;
}

int RenameByTag(char *tagName, char *newName)
{
    char oldPath[MAX_PATH_LENGTH];
    strcpy(oldPath, ".");
    FindTag(oldPath, tagName);
    char newPath[MAX_PATH_LENGTH];
    strcpy(newPath, oldPath);

    for (int i = strlen(newPath); i >= 0; i--)
    {
        if(newPath[i] == '/')   //find index where file name terminates
        {
            newPath[i+1] = '\0';    //cut off old file name
            strcat(newPath, newName);   //add new file name
            rename(oldPath, newPath);   //rename
            return 0;
        }
    }
    return 1;
}

int MoveByTags(char *startTag, char *endTag)
{
    char oldPath[MAX_PATH_LENGTH];
    strcpy(oldPath, ".");
    char newPath[MAX_PATH_LENGTH];
    strcpy(newPath, ".");
    //get paths of tags
    if(FindTag(oldPath, startTag) == 1 || FindTag(newPath, endTag) == 1)
    {
        printf("could not find tag.");
        return 1;
    }

    char command[COMMAND_LENGTH + 10];   //mv old path to new path
    snprintf(command, sizeof command, "mv %s %s", oldPath, newPath);
    printf("%s\n", command);

    FILE *p_command;    //run command
    p_command = popen(command, "r");
    pclose(p_command);
    return 0;
}


