##linux commands
// gcc -Wall -pedantic -c vector.c -o vector.o
compile vector.c into vector.o

// ar rv libvector.a vector.o
archive vector.o as library file libvector.a
so it can be used in other code

//gcc test_vector_dot_product.c -o test_vector_dot_product1 -L -lvector -I.
compile test_vector_dot_product.c

// ./test_vector_dot_product1
run test_vector_dot_product1

##c code

vector.h <- header file contains all functions and constants in library

#define SIZ 3  <- constant SIZ = 3
int add_vectors(int x[], int y[], int z[]);   <- function that accepts these parameters


test_vector_dot_product.c

#include <assert.h>  <- allow use of assert()
#include "vector.h"    <- vector library

assert(11==result);     <- if answer is != 11 return error msg
