# Submission Header
Module Code : CS1PC20

Assignment report Title : Coursework

Student Number : 31016669

Date : 1/12/2022 

Actual hrs spent for the assignment : 30 hrs

Assignment evaluation : wut?

# Project Manager Info

## How to build

run
``` 
/cs1pc20/pm$make
```
pm.exe will compile in **cs1pc20/pm/bin**

## Features Checklist


1. name checking for duplicates
    - will exit from creating any file/folder if same name already exists
    - will check given name for spaces and exit if invalid
2. directory and file structure creation
3. initialise git repo
    - add .gitignore and commit "initial commit"
    - create remote git repo in given <url>
4. feature management
    - .pm_tag files to reference feature
    - finding path given <tag name> by recursively searching all subdirectories
    - create git branch for each feature

5. rename features by tag
6. move features by tags
7. output tree diagram
    - recurses through directory tree to write folders to .puml file
    - exports .puml file as svg file using plantuml
    - only valid folders are shown in tree

8. time estimate information stored in files in subfolders
    - track_time command adds file into cd containing time esitmate
    - anytime track_time is called the time dependencies in the entire root ".pm_time" tree of the folder are updated so that subfeature times are cumulatively added into parent features using recursion
    - (it took a really long time to do this part ;-;)

9. time workload shown in tree diagram
    - added command that writes and outputs gantt chart using workload information (another recursive tree search)

## Files
number of files does not exactly matchup with features for organization purposes
main is located in project_manager.c
all other c files are linked

## Commands
its all commented on in project_manager.c

## Diagram Examples
Just to prove that it works i guess. Time is measured in days.

### tree diagram

<img src=./pm/docs/images/workloads_tree.png>

### gantt chart

<img src=./pm/docs/images/workloads_gantt.png>

