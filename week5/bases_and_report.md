
// use makefile from week4 to create directory structure with name dec2bin
make -f ../week4/framework/Makefile feature NAME=dec2bin

// test inputs for program
// it will run the bin conversion and see if the result is expected
[dec2bin_tests]

//header file for function in conv.c
[conv.h]

//lib for dec2bin
//dec2r function takes in "out" string reads a dec int input and writes bin equivalent in to the out string
[conv.c]

// calls dec2r function and prints the output
[dec2bin.c]

//running test_outputs from week4 with new test from this week
//runs the dec2bin program and checks if the conversions are correct
~/portfolio/week4/framework/test_outputs/src/test_outputs test/dec2bin_test
