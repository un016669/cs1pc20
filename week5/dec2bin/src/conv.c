#include "conv.h"
#include <stdio.h>

void dec2r(char in[], int r, char out[]) 
{
	int decval;
	sscanf(in, "%d", &decval);
	int pos=STRLEN-1;
	out[pos-1]='0';
	while (decval > 0) 
	{
		out[--pos]=(decval%r) + '0';
		decval /= r;
	}
	return;
}
